When picturing a kitchen sink, most of us imagine a perfectly symmetrical design with the drain located middle-center.

Few know, that moving the drain just inches to the right or left of center may provide benefits greater than keeping with the classic center design. Here are three key reasons you can appreciate having an off-center drain in any Havens copper or [stainless steel kitchen sinks](https://www.havensmetal.com/pages/custom-stainless-steel-sinks) 

 

# Prep, Wash, and Dry at the same time! # 
For those who prize functionality, the off-center drain has many of the same benefits as a double-bowl sink. Just like a double-bowl sink, an off-center drain allows you to prepare food on one side of the bowl, while dishes dry on the other. This feature removes the hassle of juggling the cooking and cleaning processes, leaving more time to enjoy your meal.

# Dishes at the Bottom, not Water! #

In some sinks, a wrongly placed dish can block the water flow. Havens sinks are designed to send water directly to the drain, regardless of its location. This means when using an off-center drain, you are able to place dishes in the center of the sink while still allowing sink water to be removed from the basin.

# Disposal & Storage! #

Possibly the greatest feature in an off-center sink drain is moving your plumbing and disposal into one consolidated corner underneath the sink. This not only maximizes under-sink storage capacity for chemicals and supplies, but allows for alternatives like pull out trash bins and drawers, leading to an even more convenient workspace.

With fine craftsmanship and expert design, Havens Metal is able to customize any type of copper or stainless steel kitchen sink to fit your design needs. Having an off-center drain changes the way we work and organize kitchen space, making it easier than ever to prep, wash and dry in one convenient location. 